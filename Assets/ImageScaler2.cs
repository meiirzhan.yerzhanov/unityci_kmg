﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageScaler2 : MonoBehaviour
{
   public float Scale=0.6f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
        Scale=Scale+0.05f;  
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)

        {
        Scale=Scale-0.05f;
        if (Scale <= 0.1f) {Scale=0.5f;}
        }
        GetComponent<RectTransform>().localScale = new Vector3(Scale,Scale,0);  
    }
}

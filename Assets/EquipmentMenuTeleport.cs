﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class EquipmentMenuTeleport : MonoBehaviour
{
    public GameObject GoTableList;
    
    public Button MUElements;
    public Button GUElements;
    public Button UPSVElements;
    List<Button> goUPSV = new List<Button>();
    List<GameObject> gorowUPSV = new List<GameObject>();
    List<Button> goGU = new List<Button>();
    List<GameObject> gorowsGU = new List<GameObject>();
    
    List<Button> goMU = new List<Button>();
    List<GameObject> gorowsMU = new List<GameObject>();
    List<Vector3> vectorsUPSV = new List<Vector3>(){
    new Vector3(555f, 1.073f, 541f),       //"Насосы 8Ндв-Нм-(1-4), НГ200-120-(1-2)",
    new Vector3(506f, 1.073f, 335f),       //"СЭ1250",
    new Vector3(587f, 1.073f, 472f),       //"ПТБ 10/64-(1-5)",
    new Vector3(531f, 1.073f, 533f),       //"КСУ-(1-4)",
    new Vector3(520f, 1.073f, 592f),       //"УДО-6-(1-6)",
    new Vector3(646f, 1.073f, 526f),       //"УДО-8-(1-8)",
    new Vector3(455f, 1.073f, 640f),       //"Трикантер Z5E",
    new Vector3(609f, 1.073f, 507f),       //"Газосепаратор-1.6-1",
    new Vector3(612f, 1.073f, 584f),       //"Газосепаратор-1.6-2",
    new Vector3(635f, 1.073f, 618f),       //"Газовая свеча-1",
    new Vector3(367f, 1.073f, 580f),       //"Газовая свеча-2",
    new Vector3(623f, 1.073f, 484f),       //"Газовая свеча-3",
    new Vector3(608f, 1.073f, 597f),       //"Узел Учета Газа",
    new Vector3(521f, 1.073f, 662f),       //"Дренажная емкость-1", 
    new Vector3(590f, 1.073f, 574f),       //"Дренажная емкость-2",
    new Vector3(555f, 1.073f, 302f),       //"Дренажная емкость-3",
    new Vector3(480f, 1.073f, 372f),       //"ЗРУ-6кВ",
    new Vector3(578f, 1.073f, 555f),       //"БР2.5",
    new Vector3(541f, 1.073f, 298f),       //"НБ125",
    new Vector3(631f, 1.073f, 454f),       //"Камаз",
    new Vector3(610f, 1.073f, 639f),       //"Газ66",
    new Vector3(514f, 1.073f, 509f),       //"РВС-5000-1",
    new Vector3(472f, 1.073f, 545f),       //"РВС-5000-2",
    new Vector3(437f, 1.073f, 586f),       //"РВС-5000-3",
    new Vector3(395f, 1.073f, 624f),       //"РВС-5000-4",
    new Vector3(591f, 1.073f, 361f),       //"РВС-10000-1",
    new Vector3(644f, 1.073f, 417f),       //"РВС-10000-2",
    new Vector3(567f, 1.073f, 436f),       //"РВС-400(1-2)",
    new Vector3(550f, 1.073f, 443f),       //"РВС-400-(3-4)",
    new Vector3(538f, 1.073f, 466f),       //"РВС-100-(1-2)",
    new Vector3(494f, 1.073f, 671f)};      //"8НДв-Нм-(5-8)"
    
    List<Vector3> rotationsUPSV = new List<Vector3>(){
    new Vector3(0f, 97f, 0f),       //"Насосы 8Ндв-Нм-(1-4), НГ200-120-(1-2)",
    new Vector3(0f, -164f, 0f),       //"СЭ1250",
    new Vector3(0f, -12f, 0f),       //"ПТБ 10/64-(1-5)",
    new Vector3(0f, 0f, 0f),       //"КСУ-(1-4)",
    new Vector3(0f, -100f, 0f),       //"УДО-6-(1-6)",
    new Vector3(0f, 10f, 0f),       //"УДО-8-(1-8)",
    new Vector3(0f, -105f, 0f),       //"Трикантер Z5E",
    new Vector3(0f, -11f, 0f),       //"Газосепаратор-1.6-1",
    new Vector3(0f, -11f, 0f),       //"Газосепаратор-1.6-2",
    new Vector3(0f, 30f, 0f),       //"Газовая свеча-1",
    new Vector3(0f, 64f, 0f),       //"Газовая свеча-2",
    new Vector3(0f, -122f, 0f),       //"Газовая свеча-3",
    new Vector3(0f, -117f, 0f),       //"Узел Учета Газа",
    new Vector3(0f, -95f, 0f),       //"Дренажная емкость-1", 
    new Vector3(0f, 84f, 0f),       //"Дренажная емкость-2",
    new Vector3(0f, 18f, 0f),       //"Дренажная емкость-3",
    new Vector3(0f, -80f, 0f),       //"ЗРУ-6кВ",
    new Vector3(0f, -67f, 0f),       //"БР2.5",
    new Vector3(0f, -183f, 0f),       //"НБ125",
    new Vector3(0f, 130f, 0f),       //"Камаз",
    new Vector3(0f, 9f, 0f),       //"Газ66",
    new Vector3(0f, -100f, 0f),       //"РВС-5000-1",
    new Vector3(0f, -100f, 0f),       //"РВС-5000-2",
    new Vector3(0f, -100f, 0f),       //"РВС-5000-3",
    new Vector3(0f, -100f, 0f),       //"РВС-5000-4",
    new Vector3(0f, -100f, 0f),       //"РВС-10000-1",
    new Vector3(0f, -100f, 0f),       //"РВС-10000-2",
    new Vector3(0f, -5f, 0f),       //"РВС-400(1-2)",
    new Vector3(0f, -100f, 0f),       //"РВС-400-(3-4)",
    new Vector3(0f, -100f, 0f),       //"РВС-100-(1-2)",
    new Vector3(0f, -85f, 0f)};      //"8НДв-Нм-(5-8)"

    List<Vector3> vectorsGU = new List<Vector3>(){
    new Vector3(246f, 1.24f, 814f),       //"Дренажная емкость",
    new Vector3(280f, 1.24f, 820f),       //"НБ125-(1-2)",
    new Vector3(264f, 1.24f, 824f),       //"НГС-(1-5)",
    new Vector3(342f, 1.24f, 809f)};      //"КТП",

    List<Vector3> rotationsGU = new List<Vector3>(){
    new Vector3(0f, 142f, 0f),       //"Дренажная емкость",
    new Vector3(0f, -60f, 0f),       //"НБ125-(1-2)",
    new Vector3(0f, -7f, 0f),       //"НГС-(1-5)",
    new Vector3(0f, -2f, 0f)};      //"КТП",
    List<Vector3> vectorsMU = new List<Vector3>(){
    new Vector3(508f, 1.24f, 429f),        //"ШГН-1",      
    new Vector3(612f, 1.24f, 502f),        //"ШГН-2",
    new Vector3(659f, 1.24f, 458f),        //"ШГН-3",
    new Vector3(715f, 1.24f, 510f),        //"ШГН-4",
    new Vector3(820f, 1.24f, 531f),        //"ШГН-5",
    new Vector3(688f, 1.24f, 371f),        //"ШГН-6",
    new Vector3(868f, 1.24f, 764f),        //"ШГН-7",
    new Vector3(349f, 1.24f, 466f),        //"ШГН-8",
    new Vector3(551f, 1.24f, 215f),        //"ШГН-9",
    new Vector3(380f, 1.24f, 235f),        //"ШГН-10",
    new Vector3(110f, 1.24f, 626f),        //"ШГН-11",
    new Vector3(446f, 1.24f, 464f),        //"АГЗУ",
    new Vector3(441f, 1.24f, 479f)};       //"Дренажная емкость"

    List<Vector3> rotationsMU = new List<Vector3>(){
    new Vector3(0f, 127f, 0f),        //"ШГН-1",      
    new Vector3(0f, 144f, 0f),        //"ШГН-2",
    new Vector3(0f, -90, 0f),        //"ШГН-3",
    new Vector3(0f, -63, 0f),        //"ШГН-4",
    new Vector3(0f, 36, 0f),        //"ШГН-5",
    new Vector3(0f, 67, 0f),        //"ШГН-6",
    new Vector3(0f, 30, 0f),        //"ШГН-7",
    new Vector3(0f, 110, 0f),        //"ШГН-8",
    new Vector3(0f, 52, 0f),        //"ШГН-9",
    new Vector3(0f, 150, 0f),        //"ШГН-10",
    new Vector3(0f, 127, 0f),        //"ШГН-11",
    new Vector3(0f, 76, 0f),        //"АГЗУ",
    new Vector3(0f, -10, 0f)};       //"Дренажная емкость"

    

    string[] namesUPSV = new string[]{
        "Насосы 8Ндв-Нм-(1-4), НГ200-120-(1-2)",
        "СЭ1250",
        "ПТБ 10/64-(1-5)",
        "КСУ-(1-4)",
        "УДО-6-(1-6)",
        "УДО-8-(1-8)",
        "Трикантер Z5E",
        "Газосепаратор-1.6-1",
        "Газосепаратор-1.6-2",
        "Газовая свеча-1",
        "Газовая свеча-2",
        "Газовая свеча-3",
        "Узел Учета Газа",
        "Дренажная емкость-1",
        "Дренажная емкость-2",
        "Дренажная емкость-3",
        "ЗРУ-6кВ",
        "БР2.5",
        "НБ125",
        "Камаз",
        "Газ66",
        "РВС-5000-1",
        "РВС-5000-2",
        "РВС-5000-3",
        "РВС-5000-4",
        "РВС-10000-1",
        "РВС-10000-2",
        "РВС-400(1-2)",
        "РВС-400-(3-4)",
        "РВС-100-(1-2)",
        "8НДв-Нм-(5-8)"
        };
    string[] namesGU = new string[]{
        "Дренажная емкость",
        "НБ125-(1-5)",
        "НГС-(1-2)",
        "КТП",
        };
    string[] namesMU = new string[]{
        "ШГН-1",
        "ШГН-2",
        "ШГН-3",
        "ШГН-4",
        "ШГН-5",
        "ШГН-6",
        "ШГН-7",
        "ШГН-8",
        "ШГН-9",
        "ШГН-10",
        "ШГН-11",
        "АГЗУ",
        "Дренажная емкость"
        };    
    // Start is called before the first frame update
    void Start()
    {
        MUElements.onClick.AddListener(FilterMU);
        GUElements.onClick.AddListener(FilterGU);
        UPSVElements.onClick.AddListener(FilterUPSV);

        Transform[] Buttons = GoTableList.GetComponentsInChildren<Transform>();
        foreach (Transform child in Buttons)
        {
            if (child.tag == "RowUPSV")
            {
               goUPSV.Add(child.gameObject.GetComponent<Button>());
               gorowUPSV.Add(child.gameObject);
            }
            if (child.tag == "RowGU")
            {
               goGU.Add(child.gameObject.GetComponent<Button>());
               gorowsGU.Add(child.gameObject);
            }
            if (child.tag == "RowMU")
            {
               goMU.Add(child.gameObject.GetComponent<Button>());
               gorowsMU.Add(child.gameObject);
            }
        }
        for(int i = 0; i<goUPSV.Count; i++){
            int u = i;
         
         TextMeshProUGUI[] Text = gorowUPSV[i].GetComponentsInChildren<TextMeshProUGUI>();
         Text[0].text = namesUPSV[i];
         Text[2].text = "УПСВ";
         Text[3].text = "UPSV";
         goUPSV[i].onClick.AddListener(() => GoToElementUPSV(u));
        }
        for(int i = 0; i<goGU.Count; i++){
            int u = i;
         
         TextMeshProUGUI[] Text = gorowsGU[i].GetComponentsInChildren<TextMeshProUGUI>();
         Text[0].text = namesGU[i];
         Text[2].text = "Групповая установка";
         Text[3].text = "GU";
         goGU[i].onClick.AddListener(() => GoToElementGU(u));
        }
        for(int i = 0; i<goMU.Count; i++){
            int u = i;
         
         TextMeshProUGUI[] Text = gorowsMU[i].GetComponentsInChildren<TextMeshProUGUI>();
         Text[0].text = namesMU[i];
         Text[2].text = "Замерная установка";
         Text[3].text = "MU";
         goMU[i].onClick.AddListener(() => GoToElementMU(u));
        }

    }
    void GoToElementUPSV(int u)
    {
       Values.nextLocation = new Location(vectorsUPSV[u],rotationsUPSV[u]);
       SceneManager.LoadScene("UPSV");
    }
    void GoToElementGU(int u)
    {
       Values.nextLocation = new Location(vectorsGU[u],rotationsGU[u]);
       SceneManager.LoadScene("GU");
    }
    void GoToElementMU(int u)
    {
       Values.nextLocation = new Location(vectorsMU[u],rotationsMU[u]);
       SceneManager.LoadScene("MU");
    }

    void FilterUPSV()
    {
        // Debug.Log("itsmeUPSV");
        // for(int i = 0; i<goMU.Count; i++){
        //     TextMeshProUGUI[] TextType = gorowsMU[i].GetComponentsInChildren<TextMeshProUGUI>();
        //     if (TextType[2].text == "Замерная установка"){gorowsMU[i].gameObject.SetActive(false);}
        //     else gorowsMU[i].gameObject.SetActive(true);
        // }
        // for(int i = 0; i<goGU.Count; i++){
        //     TextMeshProUGUI[] TextType = gorowsGU[i].GetComponentsInChildren<TextMeshProUGUI>();
        //     if (TextType[2].text == "Групповая установка"){gorowsGU[i].gameObject.SetActive(false);}
        //     else gorowsGU[i].gameObject.SetActive(true);
        // }
    }
    void FilterMU()
    {
        // Debug.Log(gorowsGU.Count);
        // Debug.Log(gorowsMU.Count);
        // Debug.Log("itsmeMU");
        // for(int i = 0; i<goUPSV.Count; i++){
        //     TextMeshProUGUI[] TextType = gorowsUPSV[i].GetComponentsInChildren<TextMeshProUGUI>();
        //     if (TextType[2].text == "УПСВ"){gorowsUPSV[i].gameObject.SetActive(false);}
        //     else gorowsUPSV[i].gameObject.SetActive(true);
        // }
        // for(int i = 0; i<goGU.Count; i++){
        //     TextMeshProUGUI[] TextType = gorowsGU[i].GetComponentsInChildren<TextMeshProUGUI>();
        //     if (TextType[2].text == "Групповая установка"){gorowsGU[i].gameObject.SetActive(false);}
        //     else gorowsGU[i].gameObject.SetActive(true);
        // }
    }
    void FilterGU()
    {
        // Debug.Log("itsmeGU");
        // for(int i = 0; i<goMU.Count; i++){
        //     TextMeshProUGUI[] TextType = gorowsMU[i].GetComponentsInChildren<TextMeshProUGUI>();
        //     if (TextType[2].text == "Замерная установка"){gorowsMU[i].gameObject.SetActive(false);}
        //     else gorowsMU[i].gameObject.SetActive(true);
        // }
        // for(int i = 0; i<goUPSV.Count; i++){
        //     TextMeshProUGUI[] TextType = gorowsUPSV[i].GetComponentsInChildren<TextMeshProUGUI>();
        //     if (TextType[2].text == "УПСВ"){gorowsUPSV[i].gameObject.SetActive(false);}
        //     else gorowsUPSV[i].gameObject.SetActive(true);
        // }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_color_change : MonoBehaviour
{
    public Color ItemColor;
    public Color ItemColor2;
    public Color ItemColor3;
    MeshRenderer ItemRender;
    private int counter = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Color[] colors = { ItemColor, ItemColor2, ItemColor3 };
        ItemRender = GetComponent<MeshRenderer>();
        int temp3 = Random.Range(0, 2);
        if (counter % 1000 == 0)
        {   
            
            ItemRender.material.color = colors[temp3];
            
        }
        counter = counter + 1;

    }
}
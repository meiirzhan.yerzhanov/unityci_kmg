﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnhideCanvas: MonoBehaviour
{
   public GameObject canvas;
   public bool isActive;

    public void Unhide()
    {
            canvas.SetActive(true);
    }
}

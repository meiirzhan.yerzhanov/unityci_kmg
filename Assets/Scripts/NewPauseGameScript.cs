﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewPauseGameScript : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenuUI;
    [SerializeField] private bool isPaused;
    [SerializeField] private bool isEsc;
    [SerializeField] private bool isX;



  private void Update()
  {
      if(Input.GetKeyDown(KeyCode.Escape))
      {
          isPaused = !isPaused;
          isEsc=!isEsc;
      }
      else if(Input.GetKeyDown(KeyCode.X))
      {
          isPaused = !isPaused;
          isX=!isX;
      }
      if (isPaused && isEsc)
      {
          pauseMenuUI.SetActive(true);
          ActivateMenu();
      }
      else 
      {
          pauseMenuUI.SetActive(false);
          DeactivateMenu();
      }

      if (isPaused && isX)
      {
          ActivateMenu();
      }
      else 
      {
          DeactivateMenu();
      }
  }


  void ActivateMenu()
  {
      Time.timeScale = 0;
  }
  void DeactivateMenu()
  {
      Time.timeScale = 1;
  }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour
{
    public void LoadByIndex (int sceneIndex)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene (sceneIndex);
        
        /* if (Application.loadedLevel != sceneIndex)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene (sceneIndex);
        }
        if (Application.loadedLevel == sceneIndex)
        {
            SceneManager.OnLevelWasLoaded();
        }*/

        Time.timeScale = 1;
    }
}

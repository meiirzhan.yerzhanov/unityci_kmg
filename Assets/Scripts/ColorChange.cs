﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    public Color ItemColor;
    MeshRenderer ItemRender;

    void Awake()
    {
        ItemRender = GetComponent<MeshRenderer>();
        ItemRender.material.color = ItemColor;
        
    }

}

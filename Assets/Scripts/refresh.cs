﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using CookComputing.XmlRpc;


public class refresh : MonoBehaviour
{
    
    
    private string imageString;
    public int id_number;
    int number_of_elements;
    public UnityEngine.UI.Text myGUIText;
    public Transform[] views;
    public UnityEngine.UI.Text simpleText;
    public void check_r(){
        //StartCoroutine(GetData(id_number));
        StartCoroutine(xmlrpcrequest(id_number));
        
    }
   /* IEnumerator check_refresh(){
       WWWForm imageForm = new WWWForm();
        imageForm.AddField("id", 201);
        WWW wwwImage = new WWW("http://m50168.hostch01.fornex.org/addimage.php", imageForm);
        yield return wwwImage;
        imageString = (wwwImage.text);
        print(imageString.Length);
        Debug.Log("Server: " + wwwImage.text);
        if (wwwImage.error != null)
        {
            Debug.Log("Error " + wwwImage.error);
            yield break;
        }

        if(imageString.Length>1000){
            Debug.Log("Hello");
            obj2.text = "Акт выполненных работ";
            
            
        }
        else{
            obj2.text = "";
        }

        
   }*/


    IEnumerator GetData(int id)
    {
        
        simpleText.enabled = true;
        myGUIText.text = "";
        for (int j = 0; j < 9; j++)
        {
            for (int i = 0; i < 11; i++)
                views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].text = "";
        }
        WWWForm imageForm = new WWWForm();
        imageForm.AddField("id", id);
        WWW wwwImage = new WWW("http://m51453.hostch01.fornex.org/addimage.php", imageForm);
        yield return wwwImage;
        imageString = (wwwImage.text);
        Debug.Log(imageString);
        string[] str = imageString.Split('~');
        number_of_elements = (str.Length - 1) / 22;
        print(str.Length);
        int counter = 0;
        List<int> strlist = new List<int>() { 22, 44, 66 };
        myGUIText.text = "";
        for (int j = 0; j < number_of_elements; j++)
        {
            //0-23, 23-45, 46-67, 69-89
            //0-23 23-45 45-
            for (int i = 1 + (j * 22); i < 23 + (j * 22); i++)
            {
                if (counter == 0)
                {
                    views[j].GetComponentsInChildren<TextMeshProUGUI>()[counter].text = (j + 1).ToString();
                    counter++;
                }

                if (i % 2 == 0 && i > 0 && i % 22 != 0)
                {

                    views[j].GetComponentsInChildren<TextMeshProUGUI>()[counter].text = str[i];
                    if (str[i] == "Дефектный акт")
                    {

                        Button yourButton = views[j].GetComponentsInChildren<TextMeshProUGUI>()[counter].GetComponent<Button>();
                        ColorBlock colors = yourButton.colors;
                        colors.normalColor = Color.red;
                        colors.highlightedColor = new Color32(255, 100, 100, 255);
                        yourButton.colors = colors;
                    }
                    if (str[i] == "Акт выполненных работ")
                    {

                        Button yourButton = views[j].GetComponentsInChildren<TextMeshProUGUI>()[counter].GetComponent<Button>();
                        ColorBlock colors = yourButton.colors;
                        colors.normalColor = Color.green;
                        colors.highlightedColor = new Color32(0, 177, 106, 255);
                        yourButton.colors = colors;
                    }
                    counter++;

                }
                if (i % 22 == 0 && i > 0)
                {

                    myGUIText.text += str[i] + "~";
                }
            }
            counter = 0;
        }
        simpleText.enabled = false;
        Debug.Log("No Error");
         
    }







    IEnumerator xmlrpcrequest(int id_number)
    {

        myGUIText.text = "";
        for (int j = 0; j < 9; j++)
        {
            for (int i = 0; i < 11; i++)
                views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].text = "";
        }
        simpleText.enabled = true;
        Debug.Log("XMLIN");
        yield return "hello";
        IOpenErpLogin rpcClientLogin = XmlRpcProxyGen.Create<IOpenErpLogin>(); //add  XmlRpcProxyGen.CS File from src folder if required,

    int userid = rpcClientLogin.login("odoo12", "ubquant@gmail.com", "telnet9");

    string[] list1 = new string[] { "{'fields': ['asset_id','name','description', 'maintenance_type','origin'], 'limit': 5, }" };
    string[] list2 = new string[] { "asset_id", "name", "description", "maintenance_type", "origin" };
    string[] list3 = new string[] { "description" };
    string[] list4 = new string[] { "maintenance_type" };
    string[] list5 = new string[] { "origin" };
    string[][] lists = new string[][] { list1, list2, list3, list4, list5 };

        
    XmlRpcStruct d = new XmlRpcStruct();
    d.Add("fields", new object[] { "name", "asset_id", "description" });
        d.Add("limit", 5);
        //string customer = rpcClientLogin.execute_kw("odoo12", userid, "telnet9", "mro.order", "search_read", lists2,lists);

        Debug.Log(userid.ToString());

        /*object[] args = new object[1];
        object[] subargs = new object[3];
        subargs[0] = "partner_id";
        subargs[1] = "=";
        subargs[2] = partner_id.ToString();
        int[] message_count = odooNewProxy.Search("odoo12", userid, "telnet9", "mail.notification", "search", args);*/

        System.Object[] args = new System.Object[1];
        System.Object[] subargs = new System.Object[3];
        subargs[0] = "id_equipment";
        subargs[1] = "=";
        subargs[2] = id_number.ToString();
        args[0] = subargs;

        IOpenErpexecute rpcClientexecute = XmlRpcProxyGen.Create<IOpenErpexecute>();
        System.Object[] objerp = rpcClientexecute.search("odoo12", userid, "telnet9", "mro.order", "search", args);
        System.Object[] id_asset = new System.Object[objerp.Length];
        int counteri = 0;
        foreach (System.Object o in objerp)
        {
            //Debug.Log(o != null ? o.GetType().FullName : "null");
            Debug.Log(o.ToString());
            int iii;
            Int32.TryParse(o.ToString(), out iii);
            id_asset[counteri] = iii;
            counteri++;
        }
        System.Object[] fields = new System.Object[11];
        fields[0] = "akt";
        fields[1] = "equipment_name";
        fields[2] = "defect_type";
        fields[3] = "remont_type";
        fields[4] = "price";
        fields[5] = "warranty";
        fields[6] = "executor";
        fields[7] = "responsible";
        fields[8] = "date";
        fields[9] = "reason";
        fields[10] = "description";

        IOpenErpexecuteget rpcClientexecuteget = XmlRpcProxyGen.Create<IOpenErpexecuteget>();
        System.Object[] objerp2 = rpcClientexecuteget.get_data("odoo12", userid, "telnet9", "mro.order", "read", id_asset, fields);
        List<string> eq_list = new List<string>();
        Debug.Log(objerp2.ToString());
        foreach (XmlRpcStruct o in objerp2)
        {
            eq_list.Add(o["equipment_name"].ToString());
            eq_list.Add(o["defect_type"].ToString());
            eq_list.Add(o["reason"].ToString());
            eq_list.Add(o["remont_type"].ToString());
            eq_list.Add(o["price"].ToString());
            eq_list.Add(o["warranty"].ToString());
            eq_list.Add(o["executor"].ToString());
            eq_list.Add(o["responsible"].ToString());
            eq_list.Add(o["date"].ToString());
            eq_list.Add(o["description"].ToString());
            myGUIText.text += o["akt"].ToString() + "~";
            //eq_list.Add(o["akt"]);
        }
        for (int j = 0; j<counteri; j++)
        {
            
            for (int i = 0; i <= eq_list.Count/counteri; i++)
            {
                if (i == 0)
                {
                    views[j].GetComponentsInChildren<TextMeshProUGUI>()[0].text = (j+1).ToString();
                    
                }
                else
                {
                    if (eq_list[i - 1 + j * 10] == "Дефектный акт")
                    {

                        Button yourButton = views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].GetComponent<Button>();
                        ColorBlock colors = yourButton.colors;
                        colors.normalColor = Color.red;
                        colors.highlightedColor = new Color32(255, 100, 100, 255);
                        yourButton.colors = colors;
                    }
                    if (eq_list[i - 1 + j * 10] == "Акт выполненных работ")
                    {

                        Button yourButton = views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].GetComponent<Button>();
                        ColorBlock colors = yourButton.colors;
                        colors.normalColor = Color.green;
                        colors.highlightedColor = new Color32(0, 177, 106, 255);
                        yourButton.colors = colors;
                    }
                    views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].text = eq_list[i - 1 + j * 10];
                }
            }
        }

        simpleText.enabled = false;
        Debug.Log("No Error");
        
        //Debug.Log(customer);
    }
    [XmlRpcUrl("https://odoo.ai/xmlrpc/2/common")]
public interface IOpenErpLogin : IXmlRpcProxy

{



    [XmlRpcMethod("login")]
    //[XmlRpcMethod("execute_kw")]

    int login(string dbName, string dbUser, string dbPwd);

    //string execute_kw(string dbName, int dbUser, string dbPwd, string instance, string odoomethod, Array arr, Array arr2);
}


[XmlRpcUrl("https://odoo.ai/xmlrpc/2/object")]
public interface IOpenErpexecute : IXmlRpcProxy

{

    [XmlRpcMethod("execute")]
    System.Object[] search(string dbName, int userId, string pwd, string model, string method, System.Object[] filters);


}



[XmlRpcUrl("https://odoo.ai/xmlrpc/2/object")]
public interface IOpenErpexecuteget : IXmlRpcProxy

{

    [XmlRpcMethod("execute")]
    System.Object[] get_data(string dbName, int userId, string pwd, string model, string method, object[] ids, object[] fields);

}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MiniMapPosition : MonoBehaviour
{
    public GameObject GoTableList;
    List<Button> go = new List<Button>();
    List<GameObject> gorows = new List<GameObject>();
    public CharacterController CharacterController;
    List<Vector3> vectors = new List<Vector3>(){
    new Vector3(555f, 1.073f, 541f),       //"Насосы 8Ндв-Нм-(1-4), НГ200-120-(1-2)",
    new Vector3(506f, 1.073f, 335f),       //"СЭ1250",
    new Vector3(587f, 1.073f, 472f),       //"ПТБ 10/64-(1-5)",
    new Vector3(531f, 1.073f, 533f),       //"КСУ-(1-4)",
    new Vector3(520f, 1.073f, 592f),       //"УДО-6-(1-6)",
    new Vector3(646f, 1.073f, 526f),       //"УДО-8-(1-8)",
    new Vector3(455f, 1.073f, 640f),       //"Трикантер Z5E",
    new Vector3(609f, 1.073f, 507f),       //"Газосепаратор-1.6-1",
    new Vector3(612f, 1.073f, 584f),       //"Газосепаратор-1.6-2",
    new Vector3(635f, 1.073f, 618f),       //"Газовая свеча-1",
    new Vector3(367f, 1.073f, 580f),       //"Газовая свеча-2",
    new Vector3(623f, 1.073f, 484f),       //"Газовая свеча-3",
    new Vector3(608f, 1.073f, 597f),       //"Узел Учета Газа",
    new Vector3(521f, 1.073f, 662f),       //"Дренажная емкость-1", 
    new Vector3(590f, 1.073f, 574f),       //"Дренажная емкость-2",
    new Vector3(555f, 1.073f, 302f),       //"Дренажная емкость-3",
    new Vector3(480f, 1.073f, 372f),       //"ЗРУ-6кВ",
    new Vector3(578f, 1.073f, 555f),       //"БР2.5",
    new Vector3(541f, 1.073f, 298f),       //"НБ125",
    new Vector3(631f, 1.073f, 454f),       //"Камаз",
    new Vector3(610f, 1.073f, 639f),       //"Газ66",
    new Vector3(514f, 1.073f, 509f),       //"РВС-5000-1",
    new Vector3(472f, 1.073f, 545f),       //"РВС-5000-2",
    new Vector3(437f, 1.073f, 586f),       //"РВС-5000-3",
    new Vector3(395f, 1.073f, 624f),       //"РВС-5000-4",
    new Vector3(591f, 1.073f, 361f),       //"РВС-10000-1",
    new Vector3(644f, 1.073f, 417f),       //"РВС-10000-2",
    new Vector3(567f, 1.073f, 436f),       //"РВС-400(1-2)",
    new Vector3(550f, 1.073f, 443f),       //"РВС-400-(3-4)",
    new Vector3(538f, 1.073f, 466f),       //"РВС-100-(1-2)",
    new Vector3(494f, 1.073f, 671f)};      //"8НДв-Нм-(5-8)"

    string[] names = new string[]{
        "Насосы 8Ндв-Нм-(1-4), НГ200-120-(1-2)",
        "СЭ1250",
        "ПТБ 10/64-(1-5)",
        "КСУ-(1-4)",
        "УДО-6-(1-6)",
        "УДО-8-(1-8)",
        "Трикантер Z5E",
        "Газосепаратор-1.6-1",
        "Газосепаратор-1.6-2",
        "Газовая свеча-1",
        "Газовая свеча-2",
        "Газовая свеча-3",
        "Узел Учета Газа",
        "Дренажная емкость-1",
        "Дренажная емкость-2",
        "Дренажная емкость-3",
        "ЗРУ-6кВ",
        "БР2.5",
        "НБ125",
        "Камаз",
        "Газ66",
        "РВС-5000-1",
        "РВС-5000-2",
        "РВС-5000-3",
        "РВС-5000-4",
        "РВС-10000-1",
        "РВС-10000-2",
        "РВС-400(1-2)",
        "РВС-400-(3-4)",
        "РВС-100-(1-2)",
        "8НДв-Нм-(5-8)"
        };
    public void Start()
    {
        CharacterController = GameObject.Find("RigidBodyFPSController").GetComponent<CharacterController>();
        Transform[] Buttons = GoTableList.GetComponentsInChildren<Transform>();
        foreach (Transform child in Buttons)
        {
            if (child.tag == "Row")
            {
               go.Add(child.gameObject.GetComponent<Button>());
               gorows.Add(child.gameObject);
            }
        }
        for(int i = 0; i<go.Count; i++){
            int u = i;
         go[i].onClick.AddListener(() => GoToElement(u));
         TextMeshProUGUI[] Text = gorows[i].GetComponentsInChildren<TextMeshProUGUI>();
         Text[0].text = names[i];
        }
    }

    void GoToElement(int u)
    {
      CharacterController.enabled = false;
       GameObject.Find("RigidBodyFPSController").GetComponent<Transform>().position = vectors[u];
    }

    public void Update()
    {
    }

    void LateUpdate()
    {
      CharacterController.enabled = true;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeViewButton : MonoBehaviour
{
    public Transform[] views;
    public float transitionSpeed;
    Transform currentView;
    public GameObject tabview1;
    public GameObject tabview2;
    public GameObject tabview3;
    public GameObject tabview4;
    public GameObject tabview5;
    public GameObject canvas;
 
    public void LoadView()
    {
        currentView = views[0];
        tabview1.SetActive(true);
        tabview2.SetActive(false);
        tabview3.SetActive(false);
        tabview4.SetActive(false);
        tabview5.SetActive(false);
        canvas.SetActive(true);
        Finish();
    }

    void Finish()
    {
        transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * transitionSpeed);

        Vector3 currentAngle = new Vector3(
         Mathf.LerpAngle(transform.rotation.eulerAngles.x, currentView.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
         Mathf.LerpAngle(transform.rotation.eulerAngles.y, currentView.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
         Mathf.LerpAngle(transform.rotation.eulerAngles.z, currentView.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));

        transform.eulerAngles = currentAngle;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Image_color_change : MonoBehaviour
{
    //public Color ItemColor;
    //public Color ItemColor2;
    //public Color ItemColor3;
    public Image img;
    
    private int counter = 0;
    
    void Update()
    {
        //Color[] colors = { ItemColor, ItemColor2, ItemColor3 };
         List<Color> list = new List<Color>() { new Color(255, 0, 0, 1),
        new Color(0, 255, 0, 1),
        new Color(255, 255, 0, 1)};

        int temp3 = Random.Range(0, 3);
        
        if (counter % 1000 == 0)
        {   
            
            img.color = list[temp3];
            print(temp3);
            
        }
        counter = counter + 1;

    }
}
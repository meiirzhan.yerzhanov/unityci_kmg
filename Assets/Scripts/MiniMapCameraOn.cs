﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapCameraOn : MonoBehaviour
{
    [SerializeField] private bool isActive;
    public GameObject MiniCamera;
    public GameObject MapCanvas;
    public GameObject ReticleCanvas;
    public CamSwitch CamSwitch;
    public GameObject RigidBodyFPSController;
    [SerializeField]private bool CursorLockedVar;
    
    void Start()
    {
    CamSwitch = GameObject.Find("RigidBodyFPSController").GetComponent<CamSwitch>();
    }
    
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Tab))
        {
           isActive = !isActive;
           Cursor.lockState = CursorLockMode.Locked;
           Cursor.visible = (false);
           CursorLockedVar = (true); 
          //MainCamera.transform.rotation = Quaternion.Euler(120,120,120);
        }
        if (isActive)
        {
           CamSwitch.enabled = false;
           MiniCamera.SetActive(true);
           MapCanvas.SetActive(true);
           ReticleCanvas.SetActive(false);
        }
        else 
        {
           CamSwitch.enabled = true;
           MiniCamera.SetActive(false);
           MapCanvas.SetActive(false);
           ReticleCanvas.SetActive(true);
      
           
        }
    }
}

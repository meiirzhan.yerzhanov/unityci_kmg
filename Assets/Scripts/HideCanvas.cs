﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideCanvas: MonoBehaviour
{
   public GameObject canvas;
   public bool isActive;

    public void Hide()
    {
            canvas.SetActive(false);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Location {
    public int id = 0;
    public string name = "Location name";
    public string scene = "";
    public string description = "Location Description";

    public float x = 0;
    public float y = 0;
    public float z = 0;

    public float x_rot = 0;
    public float y_rot = 0;
    public float z_rot = 0;
    public DateTime write_date;

    public Location (Vector3 location, Vector3 rotation) {
        this.x = location.x;
        this.y = location.y;
        this.z = location.z;
        this.x_rot = rotation.x;
        this.y_rot = rotation.y;
        this.z_rot = rotation.z;

    }
}
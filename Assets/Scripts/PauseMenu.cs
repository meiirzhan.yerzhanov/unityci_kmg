﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour

{
  [SerializeField] private bool isPaused;
  public GameObject CameraOff;
  public Main maingu;

  void Start () {
      maingu = GameObject.Find("Main").GetComponent<Main>();

  }

  public void Update () {
    if (Input.GetKeyDown (KeyCode.Escape)) {
      isPaused = !isPaused;
      if (isPaused) {
        maingu.player.SetActive(false);
        SceneManager.LoadSceneAsync (0, LoadSceneMode.Additive);
        CameraOff.SetActive (false);
      } else {
        SceneManager.UnloadSceneAsync (0);
        CameraOff.SetActive (true);
        maingu.player.SetActive(true);
      }
    }
  }
}
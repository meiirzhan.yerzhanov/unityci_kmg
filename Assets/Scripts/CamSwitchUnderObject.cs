﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitchUnderObject : MonoBehaviour {

    public GameObject cameraOne;
    public GameObject cameraTwo;
    public void cameraChange()
    {
        cameraOne.SetActive(false);
        cameraTwo.SetActive(true);
    }
}

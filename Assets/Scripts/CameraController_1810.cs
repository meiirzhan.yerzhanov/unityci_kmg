﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CameraController_1810 : MonoBehaviour
{   //buttons
    public Button btnPassport;
    public Button btnTORO;
    public Button btnClosePassport;
    public Button btnCloseTORO;
    //Views related
    public float transitionSpeed;
    Transform currentView;
    //canvas features
    public GameObject canvas;
    [SerializeField] private bool isActive;
    public GameObject PassportPanel;
    public GameObject PanelTORO;
    public GameObject MainCamera;
    public GameObject ElementCamera;
    public Rigidbody Rigidbody;
    public GameObject pausemenu;
    public GameObject Skvazhina;
    public TextMeshProUGUI ModelNameText;
    public TextMeshProUGUI MainStatusText;
    public TextMeshProUGUI MainData_1_text;
    public TextMeshProUGUI MainData_1_value;
    public TextMeshProUGUI MainData_2_text;
    public TextMeshProUGUI MainData_2_value;
    public TextMeshProUGUI MainData_3_text;
    public TextMeshProUGUI MainData_3_value;
    public GameObject MainStatusPanel;
    
    [SerializeField]private bool CursorLockedVar;
    MetaDataFromTXT metadata;
    arrow_mover arrowscript;
    //TableList отвечающий за поля в паспорте
    public GameObject PassportTableList;
    //TableList отвечающий за поля компонентов элемента
    public GameObject elementtablelist;
    public GameObject DataTableList;
    public GameObject ElementViews;
    List<GameObject> Children = new List<GameObject>();
    List<Transform> ViewRows = new List<Transform>();
    List<Transform> DataRows = new List<Transform>();
    List<Button> ButtonRows = new List<Button>();
    List<Color> statuscolorlist = new List<Color>() {
    new Color(0, 1, 0, 0.7f),    // green
    new Color(1, 1, 0, 0.7f),    // yellow
    new Color(1, 0, 0, 0.7f),    // red
    new Color(1, 1, 1, 0.7f)};   // white
    public int global_indicator=0;
    public void Start()
    {
        Transform[] allChildren = elementtablelist.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.tag == "Row")
            {
                Children.Add(child.gameObject);
                ButtonRows.Add(child.gameObject.GetComponent<Button>());
            }
        }
        Transform[] allViews = ElementViews.GetComponentsInChildren<Transform>();
        foreach (Transform child in allViews)
        {
            if (child.tag == "View")
            {
                ViewRows.Add(child);
            }
        }
        Transform[] allData = DataTableList.GetComponentsInChildren<Transform>();
        foreach (Transform child in allData)
        {
            if (child.tag == "DataRow")
            {
                DataRows.Add(child);
            }
        }
        //Привязываем индивидуальную функцию каждой кнопке
        for(int i = 0; i<ButtonRows.Count; i++){
            int u = i;
            ButtonRows[i].onClick.AddListener(() => ElementOpen(u));
        }
        //Кнопки меню
        btnPassport.onClick.AddListener(OpenPassport);
        btnTORO.onClick.AddListener(OpenTORO);
        btnClosePassport.onClick.AddListener(ClosePassport);
        btnCloseTORO.onClick.AddListener(CloseTORO);
        
        //Находим компонент RigidBody чтобы оставновить его движение
        Rigidbody = GameObject.Find("RigidBodyFPSController").GetComponent<Rigidbody>();
        //Приравниваем metadata для того чтобы разбить элементы row на элементы панели passport
        metadata = PassportTableList.GetComponent<MetaDataFromTXT>();
        Rigidbody.isKinematic = true;
        //Еще до старта заполняем панель паспорта, статуса и имени первым элементом (id 0) из списка и сразу же заполняем цвет
        ElementOpen(0);
        if (MainStatusText.text == "Исправно") 
            {MainStatusPanel.GetComponent<Image>().color = statuscolorlist[0];}
        else if (MainStatusText.text == "Требуется проверка")
            {MainStatusPanel.GetComponent<Image>().color = statuscolorlist[1];}
        else if (MainStatusText.text == "Не исправно")
            {MainStatusPanel.GetComponent<Image>().color = statuscolorlist[2];}
        else
            {MainStatusPanel.GetComponent<Image>().color = statuscolorlist[3];}
        //StartCoroutine(Updater());
    }

    // IEnumerator Updater(){
    //     while(True){
    //         // TODO
    //         yield return new WaitForSeconds(2);
    //     }
    // }
    //Конец функции start
    
    void OpenTORO()
    {
        PanelTORO.SetActive(true);
        PassportPanel.SetActive(false);
        Skvazhina.SetActive(false);
        
    }  
    void OpenPassport()
    {
      
        PassportPanel.SetActive(true);
        PanelTORO.SetActive(false);
        Skvazhina.SetActive(false);

    }
    void CloseTORO()
    {
        PanelTORO.SetActive(false);
    }  
    void ClosePassport()
    {
        PassportPanel.SetActive(false);
    }
    void ElementOpen(int u)
    {   global_indicator = 1;
        currentView = ViewRows[u];
        TextMeshProUGUI[] Text = Children[u].GetComponentsInChildren<TextMeshProUGUI>();
        ModelNameText.text=Text[0].text;
        MainStatusText.text=Text[1].text;
        metadata.FileName0 = Text[2].text+"_0.txt";
        metadata.FileName1 = Text[2].text+"_1.txt";
        if (Text[2].text=="АГЗУ"){MainData_1_text.text="Давление";MainData_2_text.text="Подача";MainData_3_text.text="Потребляемая мощность";}
        else if (Text[2].text=="ПСМ"){MainData_1_text.text="Давление";MainData_2_text.text="Количество подключенных скважин";MainData_3_text.text="Максимальный перепад давления";}
        else if (Text[2].text=="ЗС"){MainData_1_text.text="Объем жидкости";MainData_2_text.text="Рабочее давление установки";MainData_3_text.text="Температура";}
        else if (Text[2].text=="ШГН"){MainData_1_text.text="Потребление электроэнергии";MainData_2_text.text="Объем добычи";MainData_3_text.text="Давление";}
        else if (Text[2].text=="Скважина"){MainData_1_text.text="Потребление электроэнергии";MainData_2_text.text="Объем добычи";MainData_3_text.text="Давление";}
        else if (Text[2].text=="Балансир"){MainData_1_text.text="Потребление электроэнергии";MainData_2_text.text="";MainData_3_text.text="";}
        else if (Text[2].text=="Противовес"){MainData_1_text.text="Потребление электроэнергии";MainData_2_text.text="";MainData_3_text.text="";}
        else if (Text[2].text=="Мотор"){MainData_1_text.text="Мощность";MainData_2_text.text="Напряжение";MainData_3_text.text="Ток";}
        else if (Text[2].text=="ДЕ"){MainData_1_text.text="Плотность";MainData_2_text.text="Содержание H2S";MainData_3_text.text="Температура";}
        else if (Text[2].text=="НГС"){MainData_1_text.text="Давление";MainData_2_text.text="Объемная производительность по газу";MainData_3_text.text="Объемная производительность по нефти";}
        else if (Text[2].text=="НБ125"){MainData_1_text.text="Мощность насоса";MainData_2_text.text="Частота вращения трансмиссионного вала";MainData_3_text.text="Давление";}
        //revise pycharm cause zru and КТП
        else if (Text[2].text=="ЗРУ"){MainData_1_text.text="Мощность силового трансформатора";MainData_2_text.text="Номинальное напряжение на стороне BH";MainData_3_text.text="Номинальное напряжение на стороне PH";}
        //UPSV:
        else if (Text[2].text=="8НДВ"){MainData_1_text.text="Давление";MainData_2_text.text="Подача";MainData_3_text.text="Потребляемая мощность";}
        else if (Text[2].text=="НГ200"){MainData_1_text.text="Подача";MainData_2_text.text="Обороты";MainData_3_text.text="Мощность";}
        else if (Text[2].text=="СЭ"){MainData_1_text.text="Подача";MainData_2_text.text="Напор";MainData_3_text.text="Частота вращения";}
        else if (Text[2].text=="ПТБ"){MainData_1_text.text="Тепловая мощность";MainData_2_text.text="Температура продукта на входе в печь";MainData_3_text.text="Температура продукта на выходе из печи";}
        else if (Text[2].text=="КСУ"){MainData_1_text.text="Расчетное давление";MainData_2_text.text="Производительность по жидкости";MainData_3_text.text="Производительность по газу";}
        else if (Text[2].text=="УДО"){MainData_1_text.text="Давление";MainData_2_text.text="Температура печи";MainData_3_text.text="Процент обводненности нефти на выходе";}
        else if (Text[2].text=="ТК"){MainData_1_text.text="Давление";MainData_2_text.text="Двигатель привода барабана";MainData_3_text.text="Двигатель привода шнека";}
        else if (Text[2].text=="ГС"){MainData_1_text.text="Давление";MainData_2_text.text="Производительность по газу";MainData_3_text.text="Температура";}
        else if (Text[2].text=="ГазСвеча"){MainData_1_text.text="Давление";MainData_2_text.text="Скорость ветра";MainData_3_text.text="Длина пламени";}
        else if (Text[2].text=="УзелУчета"){MainData_1_text.text="Давление измеряемой среды";MainData_2_text.text="Температура измеряемой среды";MainData_3_text.text="Напряжение питания постоянного тока";}
        else if (Text[2].text=="БР25"){MainData_1_text.text="Давление";MainData_2_text.text="Вязкость дозируемой среды";MainData_3_text.text="Температура реагента";}
        else if ((Text[2].text=="Камаз")||(Text[2].text=="Газ66"))
        {MainData_1_text.text="Аккумулятор";MainData_2_text.text="Расход топлива";MainData_3_text.text="Давление в шинах";}
        else if ((Text[2].text=="Т100")||(Text[2].text=="Т400")||(Text[2].text=="Т5000")||(Text[2].text=="Т10000"))
        {MainData_1_text.text="Объем";MainData_2_text.text="Потребление электроэнергии";MainData_3_text.text="";}        
        if (Text[2].text=="Скважина"){Skvazhina.SetActive(true);}
        else {Skvazhina.SetActive(false);}

        //В случае с oDoo будем так делать
        // MainData_1_value.text=Text[3].text;
        // MainData_2_value.text=Text[4].text;
        // MainData_3_value.text=Text[5].text;

        //Удаляем пустые поля если нет названия
        for(int i = 0; i<DataRows.Count; i++){
            TextMeshProUGUI[] TextType = DataRows[i].GetComponentsInChildren<TextMeshProUGUI>();
            if (TextType[0].text == ""){DataRows[i].gameObject.SetActive(false);}
            else DataRows[i].gameObject.SetActive(true);
            if (TextType[1].text == ""){TextType[1].text="N/A";}
        }
        
        //Изменяем цвет главного статуса, в зависимости от нажатой функции - должен быть update и зависит от id
        MainStatusPanel.GetComponent<Image>().color = Children[u].GetComponent<Button>().colors.normalColor;
        metadata.Start();
    }
    public void Update()
    {   
        //Красим row элемента в зависимости от статуса
        for(int n = 0; n<Children.Count; n++){
            TextMeshProUGUI[] Text = Children[n].GetComponentsInChildren<TextMeshProUGUI>();
            var colors = Children[n].GetComponent<Button>().colors;
            if (Text[1].text == "Исправно") 
            {colors.normalColor = statuscolorlist[0];}
            else if (Text[1].text == "Требуется проверка")
            {colors.normalColor = statuscolorlist[1];}
            else if (Text[1].text == "Не исправно")
            {colors.normalColor = statuscolorlist[2];}
            else
            {colors.normalColor = statuscolorlist[3];}
            Children[n].GetComponent<Button>().colors = colors;   
        }
        //Отлючаем canvas кнопкой M
        if (Input.GetKeyDown(KeyCode.M))
        {
            if(Input.GetKeyDown(KeyCode.M))
            {
                isActive = !isActive;
            }
        }
        if (isActive)
        {
            canvas.SetActive(false);
        }
        else 
        {
            canvas.SetActive(true);
        }
        //Кнопка выхода из элемента
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ElementCamera.SetActive(false);
            MainCamera.SetActive(true);
            Skvazhina.SetActive(false);
            Rigidbody.isKinematic = false;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = (false);
            CursorLockedVar = (true);  
            pausemenu.SetActive(true);
        }   
    }
    void LateUpdate()
    {   
        //Плавно перемещаем камеру от элемента к элементу
        if(global_indicator==1){
            try{
            transform.position = Vector3.Lerp(transform.position, currentView.position, Time.deltaTime * transitionSpeed);
            Vector3 currentAngle = new Vector3(
            Mathf.LerpAngle(transform.rotation.eulerAngles.x, currentView.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.y, currentView.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.z, currentView.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));
            transform.eulerAngles = currentAngle;
            }
            catch{
                global_indicator = 0;
            }
        }

        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class arrow_mover : MonoBehaviour
{
    public Image img;
    private int counter = 0;
    private float ediniza = 8.1562f;
    private float average;
    private Vector3 spherePosition;
    private float arrow_location;
    private float current_location;
    private int num_potr = 100;
    private float sum = 0;
    private string unit = "м3/час";
    public TextMeshProUGUI txt2;
    public TextMeshProUGUI txt;
    private float start;
    private float end;
    public GameObject Row;
    //private float temp = 0;
    private float percentage = 0;
    private int check = 0;
    // List<Color> statuscolorlist = new List<Color>() {
    // new Color(0, 1, 0, 0.7f),    // green
    // new Color(1, 1, 0, 0.7f),    // yellow
    // new Color(1, 0, 0, 0.7f),    // red
    // new Color(1, 1, 1, 0.7f)};   // white
    List<Color> statuscolorlist = new List<Color>() {
        new Color(0, 255, 0, 0.5f),     // green
        new Color(255, 255, 0, 0.5f),   // yellow
        new Color(255, 0, 0, 0.5f),     //red
        new Color(255, 255, 255, 0.5f)};//white
    // Start is called before the first frame update
    public void Start()
    {

        txt = txt.GetComponent<TextMeshProUGUI>();
        txt2 = txt2.GetComponent<TextMeshProUGUI>();

    }

    // Update is called once per frame
    void Update()
    {

        //Debug.Log(img.transform.localPosition.x);
        //arrow_location = average * ediniza + 25.02f;
        //arrow_location = 5;
        if (counter % 50 == 0)
        {
            if (check == 0)
            {
                if (txt2.text == "Давление")
                {
                    unit = "МПа";
                    start = 0f;
                    end = 5f;
                    ediniza = 30.3765133f;
                    sum = 300;

                }
                else if (txt2.text == "Потребление электроэнергии")
                {
                    unit = "кВт";
                    start = 0f;
                    end = 18000f;
                    ediniza = 0.0227823f;
                    sum = 1000000;
                }
                else if (txt2.text == "Объем")
                {
                    unit = "м3";
                    start = 150f;
                    end = 4000f;
                    ediniza = 2.1562f;
                    sum = 200000;
                }
                else if (txt2.text == "Объем добычи")
                {
                    unit = "м3/час";
                    start = 15f;
                    end = 40f;
                    ediniza = 8.1562f;
                    sum = 3200;
                }
                else if (txt2.text == "Подача")
                {
                    unit = "м3/час";
                    start = 15f;
                    end = 40f;
                    ediniza = 8.1562f;
                    sum = 3200;

                }
                else if (txt2.text == "Потребляемая мощность")
                {
                    unit = "кВт";
                    start = 0f;
                    end = 18000f;
                    ediniza = 0.0227823f;
                    sum = 1000000;

                }
                else if (txt2.text == "Количество подключенных скважин")
                {
                    unit = "единиц";
                    start = 11f;
                    end = 11f;
                    ediniza = 0.0227823f;
                    sum = 1100;

                }
                else if (txt2.text == "Максимальный перепад давления")
                {
                    unit = "МПа";
                    start = 0f;
                    end = 5f;
                    ediniza = 0.0227823f;
                    sum = 300;

                }
                else if (txt2.text == "Объем жидкости")
                {
                    unit = "м3";
                    start = 1.8f;
                    end = 2f;
                    ediniza = 0.0227823f;
                    sum = 180;

                }
                else if (txt2.text == "Рабочее давление установки")
                {
                    unit = "МПа";
                    start = 0f;
                    end = 18000f;
                    ediniza = 0.0227823f;
                    sum = 850000;

                }
                else if (txt2.text == "Температура")
                {
                    unit = "С";
                    start = 5f;
                    end = 90f;
                    ediniza = 0.0227823f;
                    sum = 5500;

                }
                else if (txt2.text == "Мощность")
                {
                    unit = "кВт";
                    start = 0f;
                    end = 10f;
                    ediniza = 0.0227823f;
                    sum = 600;

                }
                else if (txt2.text == "Напряжение")
                {
                    unit = "В";
                    start = 375f;
                    end = 380f;
                    ediniza = 0.0227823f;
                    sum = 37500;

                }
                else if (txt2.text == "Ток")
                {
                    unit = "А";
                    start = 2f;
                    end = 5f;
                    ediniza = 0.0227823f;
                    sum = 300;

                }
                else if (txt2.text == "Плотность")
                {
                    unit = "кг/м3";
                    start = 0f;
                    end = 1000f;
                    ediniza = 0.0227823f;
                    sum = 65000;

                }
                else if (txt2.text == "Содержание H2S")
                {
                    unit = "мг/л";
                    start = 20f;
                    end = 100f;
                    ediniza = 0.0227823f;
                    sum = 6500;

                }
                else if (txt2.text == "Объемная производительность по газу")
                {
                    unit = "л";
                    start = 6300f;
                    end = 200000f;
                    ediniza = 0.0227823f;
                    sum = 1200000;

                }
                else if (txt2.text == "Объемная производительность по нефти")
                {
                    unit = "л";
                    start = 6300f;
                    end = 200000f;
                    ediniza = 0.0227823f;
                    sum = 12000000;

                }
                else if (txt2.text == "Мощность насоса")
                {
                    unit = "кВт";
                    start = 30f;
                    end = 32f;
                    ediniza = 0.0227823f;
                    sum = 3100;

                }
                else if (txt2.text == "Частота вращения трансмиссионного вала")
                {
                    unit = "Об/мин";
                    start = 0f;
                    end = 511f;
                    ediniza = 0.0227823f;
                    sum = 35000;

                }
                else if (txt2.text == "Мощность силового трансформатора")
                {
                    unit = "кВт";
                    start = 0f;
                    end = 80f;
                    ediniza = 0.0227823f;
                    sum = 6300;

                }
                else if (txt2.text == "Номинальное напряжение на стороне BH")
                {
                    unit = "В";
                    start = 220f;
                    end = 225f;
                    ediniza = 0.0227823f;
                    sum = 1000000;

                }
                else if (txt2.text == "Номинальное напряжение на стороне PH")
                {
                    unit = "В";
                    start = 220f;
                    end = 225f;
                    ediniza = 0.0227823f;
                    sum = 22200;

                }
                else if (txt2.text == "Обороты")
                {
                    unit = "Об/мин";
                    start = 0f;
                    end = 7200f;
                    ediniza = 0.0227823f;
                    sum = 510000;

                }
                else if (txt2.text == "Напор")
                {
                    unit = "л/с";
                    start = 100f;
                    end = 300f;
                    ediniza = 0.0227823f;
                    sum = 18000;

                }
                else if (txt2.text == "Частота вращения")
                {
                    unit = "Об/мин";
                    start = 0f;
                    end = 7200f;
                    ediniza = 0.0227823f;
                    sum = 520000;

                }
                else if (txt2.text == "Тепловая мощность")
                {
                    unit = "кВт";
                    start = 30f;
                    end = 32f;
                    ediniza = 0.0227823f;
                    sum = 3100;

                }
                else if (txt2.text == "Температура продукта на входе в печь")
                {
                    unit = "С";
                    start = 0f;
                    end = 20f;
                    ediniza = 0.0227823f;
                    sum = 1500;

                }
                else if (txt2.text == "Температура продукта на выходе из печи")
                {
                    unit = "С";
                    start = 70f;
                    end = 75f;
                    ediniza = 0.0227823f;
                    sum = 70000;

                }
                else if (txt2.text == "Расчетное давление")
                {
                    unit = "МПа";
                    start = 0.6f;
                    end = 2.5f;
                    ediniza = 0.0227823f;
                    sum = 150;

                }
                else if (txt2.text == "Производительность по жидкости")
                {
                    unit = "м3/ч";
                    start = 0f;
                    end = 8000f;
                    ediniza = 0.0227823f;
                    sum = 500000;

                }
                else if (txt2.text == "Производительность по газу")
                {
                    unit = "м3/ч";
                    start = 0f;
                    end = 50000f;
                    ediniza = 0.0227823f;
                    sum = 2800000;

                }
                else if (txt2.text == "Температура печи")
                {
                    unit = "С";
                    start = 0f;
                    end = 500f;
                    ediniza = 0.0227823f;
                    sum = 28000;

                }
                else if (txt2.text == "Процент обводненности нефти на выходе")
                {
                    unit = "%";
                    start = 0f;
                    end = 10f;
                    ediniza = 0.0227823f;
                    sum = 800;

                }
                else if (txt2.text == "Двигатель привода барабана")
                {
                    unit = "Об/мин";
                    start = 0f;
                    end = 7200f;
                    ediniza = 0.0227823f;
                    sum = 520000;

                }
                else if (txt2.text == "Двигатель привода шнека")
                {
                    unit = "Об/мин";
                    start = 0f;
                    end = 7200f;
                    ediniza = 0.0227823f;
                    sum = 520000;

                }
                else if (txt2.text == "Скорость ветра")
                {
                    unit = "м/с";
                    start = 0f;
                    end = 20f;
                    ediniza = 0.0227823f;
                    sum = 1500;

                }
                else if (txt2.text == "Длина пламени")
                {
                    unit = "м";
                    start = 3f;
                    end = 5f;
                    ediniza = 0.0227823f;
                    sum = 400;

                }
                else if (txt2.text == "Давление измеряемой среды")
                {
                    unit = "МПа";
                    start = 0f;
                    end = 18000f;
                    ediniza = 0.0227823f;
                    sum = 1000000;

                }
                else if (txt2.text == "Температура измеряемой среды")
                {
                    unit = "С";
                    start = 30f;
                    end = 60f;
                    ediniza = 0.0227823f;
                    sum = 4000;

                }
                else if (txt2.text == "Напряжение питания постоянного тока")
                {
                    unit = "В";
                    start = 220f;
                    end = 230f;
                    ediniza = 0.0227823f;
                    sum = 22500;

                }
                else if (txt2.text == "Вязкость дозируемой среды")
                {
                    unit = "Н·с/м2";
                    start = 20f;
                    end = 80f;
                    ediniza = 0.0227823f;
                    sum = 4000;

                }
                else if (txt2.text == "Температура реагента")
                {
                    unit = "С";
                    start = 30f;
                    end = 80f;
                    ediniza = 0.0227823f;
                    sum = 5000;

                }
                else if (txt2.text == "Аккумулятор")
                {
                    unit = "mAh";
                    start = 6000f;
                    end = 6000f;
                    ediniza = 0.0227823f;
                    sum = 600000;

                }
                else if (txt2.text == "Расход топлива")
                {
                    unit = "л/км";
                    start = 30f;
                    end = 35f;
                    ediniza = 0.0227823f;
                    sum = 3300;

                }
                else if (txt2.text == "Давление в шинах")
                {
                    unit = "МПа";
                    start = 4f;
                    end = 5f;
                    ediniza = 0.0227823f;
                    sum = 450;

                }
                else { Row.GetComponent<Image>().color = statuscolorlist[3]; }

                if (sum != 0)
                {
                    check = 1;
                }
            }

            float temp = Random.Range(start, end);
            average = sum / num_potr;
            percentage = (average / end) * 100;

            if (percentage < 20 && percentage >10)
            {
                Row.GetComponent<Image>().color = statuscolorlist[1]; // yellow
            }
            else if(percentage>20 && percentage < 80)
            {
                Row.GetComponent<Image>().color = statuscolorlist[0]; // green
            }
            else if(percentage>80 && percentage < 90)
            {
                Row.GetComponent<Image>().color = statuscolorlist[1]; // yellow
            }
            else if (percentage > 90)
            {
                Row.GetComponent<Image>().color = statuscolorlist[2]; //red 
            }
            else if (percentage < 10)
            {
                Row.GetComponent<Image>().color = statuscolorlist[2]; // red
            }
            sum = sum + temp;
            // Debug.Log("Random: "+temp);
            // Debug.Log("sum: " + sum);
            // Debug.Log("average: " + average);
            num_potr = num_potr + 1;
            //Debug.Log(arrow_location);
            //Debug.Log("Local position: "+img.transform.position.x);
            txt.text = average.ToString("N1") + " " + unit;
            
            //img.transform.Translate(arrow_location - img.transform.position.x, 0, 0, Camera.main.transform);
            //img.transform.Translate(Time.deltaTime, 0, 0, Camera.main.transform);
        }
        counter++;
        // img.transform.localPosition = new Vector3(arrow_location, img.transform.localPosition.y, img.transform.localPosition.z);
        // counter++;

    }
}

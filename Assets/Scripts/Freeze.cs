﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Freeze : MonoBehaviour
{
    public Rigidbody rb;
    [SerializeField] private bool isPaused;

    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
    }

    // Update is called once per frame
    void Update()
    {
       if (Input.GetKeyDown(KeyCode.C))
        {
            isPaused = !isPaused;
        }
        if (isPaused)
        {
            rb.isKinematic = true;
        }
        else
        {
            rb.isKinematic = false;
        }

    }

}
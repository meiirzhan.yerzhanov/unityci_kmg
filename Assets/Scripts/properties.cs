using UnityEngine;
using System.Collections.Generic;

public class properties : MonoBehaviour
{
private List<string> paramNameList = new List<string>();

public List<string> ParamNameList
{
get
{
return paramNameList;
}
set
{
paramNameList = value;
}
}

public void TheList(string value)
{
if (value != null)
{
paramNameList.Add(ParamByName(value));
paramNameList.Sort();
}
}

private string ParamByName(string d)
{
switch (d)
{

case "Мотор_[382327]":
d = "Area : 7.91 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : Мотор \nHost : Level : Генплан \nOffset : 2033.39 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : Мотор \n";
break;
case "СКД8_Балансир_[382328]":
d = "Area : 10.59 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : СКД8_Балансир \nHost : Level : Генплан \nOffset : 4422.58 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : СКД8_Балансир \n";
break;
case "Параметры_[382329]":
d = "Area : 2.36 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : Шатун \nHost : Level : Генплан \nMark : 4 \nOffset : 2603.71 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : Параметры \nДлина Лвый : 2060 mm \nДлина Правый : 2060 mm \n";
break;
case "Стойка_15.02.19_[382330]":
d = "Area : 8.34 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : Стойка 15.02.19 \nHost : Level : Генплан \nOffset : 500 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : Стойка 15.02.19 \n";
break;
case "Фундаментная_плита_500мм_[382333]":
d = "Absorptance : 0.1 \nArea : 5.99 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCore Thickness : 500 mm \nDefault Thickness : 500 mm \nElevation at Bottom : -200 mm \nElevation at Bottom Core : -200 mm \nElevation at Bottom Survey : -200 mm \nElevation at Top : 300 mm \nElevation at Top Core : 300 mm \nElevation at Top Survey : 300 mm \nFamily Name : Foundation Slab \nFoundation Thickness : 500 mm \nHeight Offset From Level : 300 mm \nLength : 1210 mm \nPerimeter : 12322.18 mm \nThickness : 500 mm \nType Name : Фундаментная плита 500мм \nWidth : 4951.09 mm \n";
break;
case "CКД8_Сальниковый_Штык_[382340]":
d = "Area : 0.24 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : CКД8_Сальниковый_Штык \nHost : Level : Генплан \nOffset : 3000.82 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : CКД8_Сальниковый_Штык \n";
break;
case "СКД8_Мотор_Колесо_Б_[382341]":
d = "Area : 1.03 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : СКД8_Мотор_Колесо_Б \nHost : Level : Генплан \nOffset : 2034.93 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : СКД8_Мотор_Колесо_Б \n";
break;
case "Фундаментная_плита_150_мм_[382343]":
d = "Absorptance : 0.1 \nArea : 3.31 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCore Thickness : 150 mm \nDefault Thickness : 150 mm \nElevation at Bottom : 1403.39 mm \nElevation at Bottom Core : 1403.39 mm \nElevation at Bottom Survey : 1403.39 mm \nElevation at Top : 1553.39 mm \nElevation at Top Core : 1553.39 mm \nElevation at Top Survey : 1553.39 mm \nFamily Name : Foundation Slab \nFoundation Thickness : 150 mm \nHeight Offset From Level : 553.39 mm \nLength : 1140 mm \nPerimeter : 8080 mm \nThickness : 150 mm \nType Name : Фундаментная плита 150 мм \nWidth : 2900 mm \n";
break;
case "W250X73_[382351]":
d = "Assembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nBase Offset : 300 mm \nCode Name : AISC 14.1 \nFamily Name : M_W Формы-Колонна \nLength : 1200 mm \nOmniClass Number : 23.25.30.11.14.11 \nOmniClass Title : Columns \nSection Name Key : W10x49 \nTop Offset : 500 mm \nType Name : W250X73 \n";
break;
case "W250X73_[382353]":
d = "Assembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nBase Offset : 300 mm \nCode Name : AISC 14.1 \nFamily Name : M_W Формы-Колонна \nLength : 1200 mm \nOmniClass Number : 23.25.30.11.14.11 \nOmniClass Title : Columns \nSection Name Key : W10x49 \nTop Offset : 500 mm \nType Name : W250X73 \n";
break;
case "W250X73_[382355]":
d = "Assembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nBase Offset : 300 mm \nCode Name : AISC 14.1 \nFamily Name : M_W Формы-Колонна \nLength : 1200 mm \nOmniClass Number : 23.25.30.11.14.11 \nOmniClass Title : Columns \nSection Name Key : W10x49 \nTop Offset : 500 mm \nType Name : W250X73 \n";
break;
case "W250X73_[382357]":
d = "Assembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nBase Offset : 300 mm \nCode Name : AISC 14.1 \nFamily Name : M_W Формы-Колонна \nLength : 1200 mm \nOmniClass Number : 23.25.30.11.14.11 \nOmniClass Title : Columns \nSection Name Key : W10x49 \nTop Offset : 500 mm \nType Name : W250X73 \n";
break;
case "W250X73_[382359]":
d = "Assembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nBase Offset : 300 mm \nCode Name : AISC 14.1 \nFamily Name : M_W Формы-Колонна \nLength : 1200 mm \nOmniClass Number : 23.25.30.11.14.11 \nOmniClass Title : Columns \nSection Name Key : W10x49 \nTop Offset : 500 mm \nType Name : W250X73 \n";
break;
case "W250X73_[382361]":
d = "Assembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nBase Offset : 300 mm \nCode Name : AISC 14.1 \nFamily Name : M_W Формы-Колонна \nLength : 1200 mm \nOmniClass Number : 23.25.30.11.14.11 \nOmniClass Title : Columns \nSection Name Key : W10x49 \nTop Offset : 500 mm \nType Name : W250X73 \n";
break;
case "Противовес_15.02.19_[382365]":
d = "Area : 5.81 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : Противовес 15.02.19 \nHost : Level : Генплан \nL : 850 mm \nOffset : 1873.39 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : Противовес 15.02.19 \nWidth btw el : 850 mm \n";
break;
case "Скважина_Верх_15.02.19_[382366]":
d = "Area : 30.42 m2 \nAssembly Code :  \nAssembly Description :  \nAssembly Name : ШГН \nCode Name :  \nFamily Name : Скважина Верх 15.02.19 \nHost : Level : Генплан \nOffset : -1120 mm \nOmniClass Number :  \nOmniClass Title :  \nType Name : Скважина Верх 15.02.19 \nГлубина Кондуктора : 2200 mm \nГлубина Направления : 300 mm \nГлубина Тех. Колонны : 11500 mm \nГлубина Эксплуатационной колонны : 14500 mm \n";
break;


}
return d;
}
}


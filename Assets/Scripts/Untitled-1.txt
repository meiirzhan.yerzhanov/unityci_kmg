curl 'https://shanyraq.valis.kz/web/dataset/call_kw/property.meter.log/read'
'Origin: https://shanyraq.valis.kz' 
 -H 'Content-Type: application/json'
 -H 'Accept: application/json, text/javascript, */*; q=0.01' 
-H 'Cookie: frontend_lang=ru_RU; session_id=8531ea1e30b7fdbcc4d7e45cabecbfb5e9547aff;'
 
 
--data-binary '
{"jsonrpc":"2.0","method":"call",
"params":{"args":[[42938],["meter_id","t_in","t_out","t_delta","q_in","q_out","q_delta","v_in","v_out","v_delta","p_in","p_out","p_delta","used_value","month_log_sum","this_month","year_log_sum","this_year","uom_id","display_name"]],"model":"property.meter.log","method":"read","kwargs":{"context":{"lang":"ru_RU","tz":"Asia/Almaty","uid":1,"params":{"action":534},"bin_size":true}}},"id":675228385}

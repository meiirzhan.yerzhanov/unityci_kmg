﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using TMPro;

public class MetaDataFromTXT : MonoBehaviour
{
    public GameObject TableList; 
    public string FileName0;
    public string FileName1;
    string fileLocation = "Assets/Passports/";
    //"Assets/Passports/Скважина ШГН_1.txt"
    public void Start()
    {
        List<GameObject> Children = new List<GameObject>();
        List<string> my_list_left = new List<string>();
        List<string> my_list_right = new List<string>();
        Transform[] allChildren = TableList.GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            if (child.tag == "Row")
            {
                Children.Add(child.gameObject);
            }
        }
        string twostrings0 = string.Concat(fileLocation, FileName0); 
        string twostrings1 = string.Concat(fileLocation, FileName1);
        ReadString(twostrings0, my_list_left);
        ReadString(twostrings1, my_list_right);
        for (int i=0; i<my_list_left.Count ;i++)
        {
            TextMeshProUGUI[] Text = Children[i].GetComponentsInChildren<TextMeshProUGUI>();

            Text[1].text = my_list_right[i];
            Text[0].text = my_list_left[i];
        }
    }

    public void ReadString(string path, List<string> my_list_new)
    {
        StreamReader reader = new StreamReader(path); 
        string text;
        do
        {
            text = reader.ReadLine();
            my_list_new.Add(text);
        } while (text != null); 

        my_list_new.RemoveAt(my_list_new.Count - 1);
        reader.Close();
    }
}

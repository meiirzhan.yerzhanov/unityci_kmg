﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class pressure_info : MonoBehaviour
{
    public Image img;
    public TextMeshProUGUI txt;


    private int counter = 0;
    private double average = 0;
    private double num_potr = 10;
    private float sum = 60;
    private double number = 0.6;
    public string unit;

    void Start()
    {
       txt = GetComponent<TextMeshProUGUI>();
    }
    void Update()
    {
        List<Color> list = new List<Color>() 
        { 
            new Color(255, 0, 0, 1),
            new Color(0, 255, 0, 1),
            new Color(255, 255, 0, 1)
        };

        int temp3 = Random.Range(0, 3);
        int temp = Random.Range(0, 19);
        if (counter % 250 == 0)
        {
            sum = sum + temp;
            num_potr = num_potr + number;
            average = sum / num_potr;

            if (average > 12)
            {
                img.color = list[0];

            }
            else if ((average < 12) && (average > 10))
            {
                img.color = list[2];
            }
            else if (average < 3)
            {
                img.color = list[2];
            }
            else
            {
                img.color = list[1];
            }

            txt.text = average.ToString("N1") + " "+unit;

            //print(average);

        }
        counter = counter + 1;

    }
}
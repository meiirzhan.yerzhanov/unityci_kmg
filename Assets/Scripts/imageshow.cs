﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class imageshow : MonoBehaviour
{
     
     
     private string imageString;
     public GameObject image;
     RawImage img;
     public int id_number;
     public UnityEngine.UI.Text myGUIText;

    void Start()
    {
        img = image.GetComponent<RawImage>();
    } 


     public void show_close(){
         
         if(image.active){
             image.SetActive(false);
             img.texture = null;
         }
         else{
             img.texture = null;
             image.SetActive(true);
             StartCoroutine(GetImage(200));
         }
     }

     public void show_close2(){
         
         if(image.active){
             image.SetActive(false);
             //img.texture = null;
         }
         else{
            Debug.Log("Hello my id is: "+id_number);
             //img.texture = null;
             image.SetActive(true);
             //GetImage_i(id_number);
         }
     }



    void GetImage_i(int id)
    {
        string[] str = myGUIText.text.Split('~');
        Debug.Log("Hello it's me "+str.Length);
        byte[] Bytes = System.Convert.FromBase64String(str[id]);
        Texture2D texture = new Texture2D(1, 1);
        texture.LoadImage(Bytes);
        //UnityEngine.GUI.DrawTexture(new Rect(200, 20, 440, 440), texture, ScaleMode.ScaleToFit, true, 1f);

        img.texture = texture;
        img.SetNativeSize();
    }

     IEnumerator GetImage(int id)
    {
        WWWForm imageForm = new WWWForm();
        imageForm.AddField("id", id);
        WWW wwwImage = new WWW("http://m50168.hostch01.fornex.org/addimage.php", imageForm);
        yield return wwwImage;
        imageString = (wwwImage.text);
        print(imageString.Length);
        Debug.Log("Server: " + wwwImage.text);
        if (wwwImage.error != null)
        {
            Debug.Log("Error " + wwwImage.error);
            yield break;
        }
        byte[] Bytes = System.Convert.FromBase64String(imageString);
        Texture2D texture = new Texture2D(1, 1);
        texture.LoadImage(Bytes);
        //UnityEngine.GUI.DrawTexture(new Rect(200, 20, 440, 440), texture, ScaleMode.ScaleToFit, true, 1f);
        
        img.texture = texture;
        img.SetNativeSize();
    }
}


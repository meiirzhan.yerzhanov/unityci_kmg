﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSwitch : MonoBehaviour {

    public GameObject MainCamera;
    public Rigidbody Rigidbody;
    [SerializeField]private bool CursorLockedVar;
    public GameObject pausemenu;

    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        switchCamera();
    }
    void switchCamera()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
            if (Input.GetMouseButtonDown(0) && Physics.Raycast(ray, out hit, 10.0f))
            {
                BoxCollider bc = hit.collider as BoxCollider;
                CamSwitchUnderObject clickScript = hit.collider.GetComponent<CamSwitchUnderObject>();
                if (clickScript && bc !=null)
                {
                    pausemenu.SetActive(false);
                    clickScript.cameraChange();  
                    // Rigidbody.isKinematic = true;
                    Cursor.lockState = CursorLockMode.None;
                    Cursor.visible = (true);
                    CursorLockedVar = (false);
                    
                }
            }  
        }
    }
}

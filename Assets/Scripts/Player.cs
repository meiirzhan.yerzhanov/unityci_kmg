﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Player : MonoBehaviour
{
    // Start is called before the first frame update
    public Main main;
    void Start()
    {
        LoadInitialPosition();
        main = GameObject.Find("Main").GetComponent<Main>();   
    }

    void LoadInitialPosition(){
        if(Values.nextLocation != null){
            Location l = Values.nextLocation;
            transform.position = new Vector3(l.x, l.y, l.z);
            transform.rotation = Quaternion.Euler(l.x_rot, l.y_rot, l.z_rot);
            // if(main.type == "GU" || main.type == "MU") {
               // GetComponent<RigidbodyFirstPersonController>().enabled = true;
            // }
            // if(main.type == "UPSV") GetComponent<FirstPersonController>().enabled = true;
        }
    }

    void Update(){
        // Debug.Log(transform.rotation);
    }
}

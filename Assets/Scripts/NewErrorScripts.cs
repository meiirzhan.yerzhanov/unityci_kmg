﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class NewErrorScripts : MonoBehaviour
{


    public Image img2;
    public TextMeshProUGUI txt2;
    private int counter2 = 0;
    private float average2 = 0;
    private int num_potr2 = 5;
    private float sum2 = 52;

    private int vol_press = 60;


    public Image img;
    public TextMeshProUGUI txt;
    public GameObject obj;
    public GameObject obj2;
    private Renderer rend;
    private Renderer rend2;

    private int counter = 0;
    private double average = 0;
    private double num_potr = 10;
    private float sum = 60;
    private double number = 0.6;
    public float width = 150.0f;
    public float widthgreen = 0.0f;
    public int check = 0;
    public int check2 = 0;
    public int ggg = 3;
    // Start is called before the first frame update
    void Start()
    {
        txt2 = txt2.GetComponent<TextMeshProUGUI>();

        txt = txt.GetComponent<TextMeshProUGUI>();
        rend = obj.GetComponent<Renderer>();
        rend2 = obj2.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        pressure_info();
        volume_info();
    }



    private void volume_info()
    {
        List<Color> list = new List<Color>() 
        { 
            new Color(255, 0, 0, 1),
            new Color(0, 255, 0, 1),
            new Color(255, 255, 0, 1)
        };

        
        float temp = Random.Range(0, vol_press);

        if (counter2 % 100 == 0)
        {
            sum2 = sum2 + temp;
            num_potr2 = num_potr2 + 1;
            average2 = sum2 / num_potr2;

            if ((average2 < 10) || (average2 > 52))
            {
                img2.color = list[0];
            }
            else if ((average2 < 20) && (average2 > 10))
            {
                img2.color = list[2];
            }
            else
            {
                img2.color = list[1];
            }

            txt2.text = average2.ToString("N1") + " м3/ч";

        }
        counter2 = counter2 + 1;

    }


     private void pressure_info()
    {
        
        List<Color> list = new List<Color>() 
        { 
            new Color(255, 0, 0, 1),
            new Color(0, 255, 0, 1),
            new Color(255, 255, 0, 1),
            new Color(0.8705882f, 0.8705882f, 0.8705882f, 1)
        };
       
        int temp3 = Random.Range(0, 3);
        int temp = Random.Range(0, 19);
        if (counter % 250 == 0)
        {
            sum = sum + temp;
            num_potr = num_potr + number;
            average = sum / num_potr;

            if (average > 12)
            {
                img.color = list[0];
                rend.material.color = list[0];
                rend2.material.color = list[0];
                vol_press = 6;
            }
            else if ((average < 12) && (average > 10))
            {
                img.color = list[2];
                rend.material.SetInt("_Outline", ggg);
                rend.material.color = list[3];
                rend2.material.SetInt("_Outline", ggg);
                rend2.material.color = list[3];
                check = 1;
            }
            else if (average < 3)
            {
                img.color = list[2];
                rend2.material.SetInt("_Outline", ggg);
                rend2.material.color = list[3];
                rend.material.SetInt("_Outline", ggg);
                rend.material.color = list[3];
                check = 1;
            }
            else
            {
                img.color = list[1];
                rend2.material.SetFloat("_Outline", widthgreen);
                rend2.material.color = list[3];
                rend.material.SetFloat("_Outline", widthgreen);
                rend.material.color = list[3];
                check = 0;
            }

            txt.text = average.ToString("N1") + " атм";

            //print(average);

        }
        if (counter % 100 == 0)
        {
        if (check == 1){
            if (check2 == 0){
            rend2.material.SetInt("_Outline", ggg);
            rend.material.SetInt("_Outline", ggg);
            check2 = 1;
            }
            else{
                rend2.material.SetFloat("_Outline", widthgreen);
                rend.material.SetFloat("_Outline", widthgreen);
                check2 = 0;
            }
        }
        }
        counter = counter + 1;

    }
}

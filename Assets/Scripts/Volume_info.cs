﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Volume_info : MonoBehaviour
{
    public Image img;
    public TextMeshProUGUI txt;
    private int counter = 0;
    private float average = 0;
    private int num_potr = 5;
    private float sum = 52;
    public string unit;

    void Start()
    {
       txt = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        List<Color> list = new List<Color>() 
        { 
            new Color(255, 0, 0, 1),
            new Color(0, 255, 0, 1),
            new Color(255, 255, 0, 1)
        };

        int temp3 = Random.Range(0, 3);
        float temp = Random.Range(0, 60);

        if (counter % 200 == 0)
        {
            sum = sum + temp;
            num_potr = num_potr + 1;
            average = sum / num_potr;

            if ((average < 10) || (average > 52))
            {
                img.color = list[0];
            }
            else if ((average < 20) && (average > 10))
            {
                img.color = list[2];
            }
            else
            {
                img.color = list[1];
            }

            txt.text = average.ToString("N1") + " "+unit;

        }
        counter = counter + 1;

    }
}

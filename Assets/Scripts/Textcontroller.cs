﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using CookComputing.XmlRpc;
using System.Reflection;
using System.Linq;
using System.Threading;

public class Textcontroller : MonoBehaviour
{


    public RectTransform rectComponent;
    private RectTransform rectComponent2;
    private float rotateSpeed = 300f;
    public GameObject image_object;


    public Transform[] views;
    private string imageString;
    TextMeshProUGUI[] img;
    string imageString_array;
    int number_of_elements;
    public int id_number_eq;

    public UnityEngine.UI.Text myGUIText;




    // Start is called before the first frame update


    Thread _thread;
    bool _threadRunning;
    private string image_text = "";
    private List<string> eq_list = new List<string>();
    private int counteri = 0;
    private int check_thread = 0;
    private int check_thread2 = 0;
    void Start()
    {
        rectComponent2 = rectComponent.GetComponent<RectTransform>();

        myGUIText.enabled = false;
        fake_xmprpc_request();
        /*_thread = new Thread(xmlrpcrequest);
        _thread.Start();*/


    }

    // Update is called once per frame
    void Update()
    {
        if (check_thread2 == 1)
        {
            myGUIText.text = image_text;
            put_values();
            image_object.SetActive(false);
            check_thread2 = 0;
        }

        rectComponent2.Rotate(0f, 0f, -rotateSpeed * Time.deltaTime);
    }

    public void fake_xmprpc_request()
    {



        eq_list.Add("ШГН");
        eq_list.Add("Перегрев");
        eq_list.Add("Перегрев");
        eq_list.Add("Замена");
        eq_list.Add("52000");
        eq_list.Add("2 года");
        eq_list.Add("ИП АВС");
        eq_list.Add("Дармешов");
        eq_list.Add("2019-07-18");
        eq_list.Add("Дефектный акт");
        
        
        eq_list.Add("Фитинг");
        eq_list.Add("Коррозия");
        eq_list.Add("Ненадлежащая эксплуатация");
        eq_list.Add("Замена");
        eq_list.Add("25000");
        eq_list.Add("1 год");
        eq_list.Add("Маликов Т.");
        eq_list.Add("Дармешов Р.");
        eq_list.Add("2019-07-18");
        eq_list.Add("Дефектный акт");
        
        eq_list.Add("Фитинг");
        eq_list.Add("Коррозия");
        eq_list.Add("перегрев");
        eq_list.Add("Замена");
        eq_list.Add("25000");
        eq_list.Add("1 год");
        eq_list.Add("Маликов Т.");
        eq_list.Add("Дармешов Р.");
        eq_list.Add("2019-07-23");
        eq_list.Add("Акт выполненных работ");
        
        eq_list.Add("Мотор");
        eq_list.Add("Троит");
        eq_list.Add("Ненадлежащая эксплуатация");
        eq_list.Add("Замена блока цилиндра");
        eq_list.Add("65000");
        eq_list.Add("3 года");
        eq_list.Add("СТО");
        eq_list.Add("Ережепов К.");
        eq_list.Add("2019-08-28");
        eq_list.Add("Акт выполненных работ");
        
        eq_list.Add("ШГН");
        eq_list.Add("Перегрев");
        eq_list.Add("Ненадлежащая эксплуатация");
        eq_list.Add("Высушка");
        eq_list.Add("60000");
        eq_list.Add("3 года");
        eq_list.Add("Иманов Р.");
        eq_list.Add("Дармешов О.");
        eq_list.Add("2019-08-20");
        eq_list.Add("Акт выполненных работ");
        
        counteri = 5;
        put_values();
        image_object.SetActive(false);
    }
    public void change_thread_state()
    {
        /*_thread = new Thread(xmlrpcrequest);
        _thread.Start();
        image_object.SetActive(true);
        for (int j = 0; j < 9; j++)
        {
            for (int i = 0; i < 11; i++)
                views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].text = "";
        }*/
    }





    [XmlRpcUrl("https://odoo.ai/xmlrpc/2/common")]
    public interface IOpenErpLogin : IXmlRpcProxy

    {



        [XmlRpcMethod("login")]
        //[XmlRpcMethod("execute_kw")]

        int login(string dbName, string dbUser, string dbPwd);

        //string execute_kw(string dbName, int dbUser, string dbPwd, string instance, string odoomethod, Array arr, Array arr2);
    }


    [XmlRpcUrl("https://odoo.ai/xmlrpc/2/object")]
    public interface IOpenErpexecute : IXmlRpcProxy

    {

        [XmlRpcMethod("execute")]
        System.Object[] search(string dbName, int userId, string pwd, string model, string method, System.Object[] filters);


    }



    [XmlRpcUrl("https://odoo.ai/xmlrpc/2/object")]
    public interface IOpenErpexecuteget : IXmlRpcProxy

    {

        [XmlRpcMethod("execute")]
        System.Object[] get_data(string dbName, int userId, string pwd, string model, string method, object[] ids, object[] fields);

    }

    private void xmlrpcrequest()
    {
        counteri = 0;
        image_text = "";
        eq_list.Clear();
        _threadRunning = true;
        Debug.Log("XMLIN");
        IOpenErpLogin rpcClientLogin = XmlRpcProxyGen.Create<IOpenErpLogin>(); //add  XmlRpcProxyGen.CS File from src folder if required,
        int userid = rpcClientLogin.login("odoo12", "ubquant@gmail.com", "telnet9");


        Debug.Log(userid.ToString());

        System.Object[] args = new System.Object[1];
        System.Object[] subargs = new System.Object[3];
        subargs[0] = "id_equipment";
        subargs[1] = "=";
        subargs[2] = 1000;
        args[0] = subargs;
        IOpenErpexecute rpcClientexecute = XmlRpcProxyGen.Create<IOpenErpexecute>();
        System.Object[] objerp = rpcClientexecute.search("odoo12", userid, "telnet9", "mro.order", "search", args);
        System.Object[] id_asset = new System.Object[objerp.Length];

        foreach (System.Object o in objerp)
        {
            Debug.Log(o.ToString());
            int iii;
            Int32.TryParse(o.ToString(), out iii);
            id_asset[counteri] = iii;
            counteri++;
        }
        System.Object[] fields = new System.Object[11];
        fields[0] = "akt";
        fields[1] = "equipment_name";
        fields[2] = "defect_type";
        fields[3] = "remont_type";
        fields[4] = "price";
        fields[5] = "warranty";
        fields[6] = "executor";
        fields[7] = "responsible";
        fields[8] = "date";
        fields[9] = "reason";
        fields[10] = "description";

        IOpenErpexecuteget rpcClientexecuteget = XmlRpcProxyGen.Create<IOpenErpexecuteget>();
        System.Object[] objerp2 = rpcClientexecuteget.get_data("odoo12", userid, "telnet9", "mro.order", "read", id_asset, fields);

        Debug.Log(objerp2.ToString());
        foreach (XmlRpcStruct o in objerp2)
        {
            eq_list.Add(o["equipment_name"].ToString());
            eq_list.Add(o["defect_type"].ToString());
            eq_list.Add(o["reason"].ToString());
            eq_list.Add(o["remont_type"].ToString());
            eq_list.Add(o["price"].ToString());
            eq_list.Add(o["warranty"].ToString());
            eq_list.Add(o["executor"].ToString());
            eq_list.Add(o["responsible"].ToString());
            eq_list.Add(o["date"].ToString());
            eq_list.Add(o["description"].ToString());
            image_text += o["akt"].ToString() + "~";

        }
        Debug.Log("No Error");

        check_thread2 = 1;

        _threadRunning = false;
        _thread.Abort();
    }

    private void put_values()
    {
        

        for (int j = 0; j < counteri; j++)
        {


            for (int i = 0; i <= eq_list.Count / counteri; i++)
            {
                if (i == 0)
                {
                    views[j].GetComponentsInChildren<TextMeshProUGUI>()[0].text = (j + 1).ToString();

                }
                else
                {
                    if (eq_list[i - 1 + j * 10] == "Дефектный акт")
                    {

                        Button yourButton = views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].GetComponent<Button>();
                        ColorBlock colors = yourButton.colors;
                        colors.normalColor = Color.red;
                        colors.highlightedColor = new Color32(255, 100, 100, 255);
                        yourButton.colors = colors;
                    }
                    if (eq_list[i - 1 + j * 10] == "Акт выполненных работ")
                    {

                        Button yourButton = views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].GetComponent<Button>();
                        ColorBlock colors = yourButton.colors;
                        colors.normalColor = Color.green;
                        colors.highlightedColor = new Color32(0, 177, 106, 255);
                        yourButton.colors = colors;
                    }
                    views[j].GetComponentsInChildren<TextMeshProUGUI>()[i].text = eq_list[i - 1 + j * 10];
                }
            }
        }
    }







}

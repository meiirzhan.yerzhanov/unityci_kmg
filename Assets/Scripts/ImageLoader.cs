﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ImageLoader : MonoBehaviour
{
    public RawImage img;
    //public string url = "https://drive.google.com/file/d/11Ruezigv8siEfjn8R2-86HqLCaAEkWVD/view";
    //public Renderer thisRenderer;
    
    void Awake()
    {
        img = this.gameObject.GetComponent<RawImage>();

    }
    
    IEnumerator Start()
    {
        Dictionary<string, string> headers = new Dictionary<string, string>(); 
        headers.Add("User-Agent", "Mozilla / 5.0(Windows NT 10.0; WOW64) AppleWebKit / 537.36(KHTML, like Gecko) Chrome / 55.0.2883.87 Safari / 537.36");
        WWW www = new WWW ("https://drive.google.com/file/d/11Ruezigv8siEfjn8R2-86HqLCaAEkWVD", null, headers);
        yield return www ;
        img.texture = www.texture;

    }
    //void Start()
    //{
        //StartCoroutine(LoadFromLikeCoroutine());
        //thisRenderer.material.color = Color.red;

    //}

   
    void Update()
    {
        
    }

    /* private IEnumerator LoadFromLikeCoroutine()
    {
        WWW wwwloader = new WWW(url);
        yield return wwwloader;
        
        thisRenderer.material.color = Color.white;
        thisRenderer.material.mainTexture = wwwloader.texture;
    }*/
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class pressure_info2 : MonoBehaviour
{
    public Image img;
    public TextMeshProUGUI txt;
    public GameObject obj;
    private Renderer rend;

    private int counter = 0;
    private double average = 0;
    private double num_potr = 10;
    private float sum = 60;
    private double number = 0.6;
    public float width = 150.0f;
    public float widthgreen = 0.0f;
    public int check = 0;
    public int check2 = 0;
    public int ggg = 3;
    public string unit;

    void Start()
    {
       txt = GetComponent<TextMeshProUGUI>();
       rend = obj.GetComponent<Renderer>();
       

       
    }
    void Update()
    {
        
        List<Color> list = new List<Color>() 
        { 
            new Color(255, 0, 0, 1),
            new Color(0, 255, 0, 1),
            new Color(255, 255, 0, 1),
            new Color(0.8705882f, 0.8705882f, 0.8705882f, 1)
        };
       
        int temp3 = Random.Range(0, 3);
        int temp = Random.Range(0, 15);
        if (counter % 250 == 0)
        {
            sum = sum + temp;
            num_potr = num_potr + number;
            average = sum / num_potr;

            if (average > 12)
            {
                img.color = list[0];
                rend.material.color = list[0];
            }
            else if ((average < 12) && (average > 10))
            {
                img.color = list[2];
                rend = obj.GetComponent<Renderer>();
                rend.material.SetInt("_Outline", ggg);
                rend.material.color = list[3];
                check = 1;
            }
            else if (average < 3)
            {
                img.color = list[2];
                rend = obj.GetComponent<Renderer>();
                rend.material.SetInt("_Outline", ggg);
                rend.material.color = list[3];
                check = 1;
            }
            else
            {
                img.color = list[1];
                rend = obj.GetComponent<Renderer>();
                rend.material.SetFloat("_Outline", widthgreen);
                rend.material.color = list[3];
                check = 0;
            }

            txt.text = average.ToString("N1") + " "+unit;

            //print(average);

        }
        if (counter % 100 == 0)
        {
        if (check == 1){
            if (check2 == 0){
            rend = obj.GetComponent<Renderer>();
            rend.material.SetInt("_Outline", ggg);
            check2 = 1;
            }
            else{
                rend = obj.GetComponent<Renderer>();
                rend.material.SetFloat("_Outline", widthgreen);
                check2 = 0;
            }
        }
        }
        counter = counter + 1;

    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ElectroCons_info : MonoBehaviour
{
    public Image img;
    public TextMeshProUGUI txt;
    private int counter = 0;
    private int average = 0;
    private int num_potr = 100;
    private int sum = 1700000;
    public string unit;
    
    void Start()
    {
       txt = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
        List<Color> list = new List<Color>() { new Color(255, 0, 0, 1),
        new Color(0, 255, 0, 1),
        new Color(255, 255, 0, 1)};
        int temp3 = Random.Range(0, 3);
        int temp = Random.Range(0, 18000);
        if (counter % 100 == 0)
        {
            sum = sum + temp;
            num_potr = num_potr + 1;
            average = sum / num_potr;
            if ((average < 3000)||(average>18000))
            {
                img.color = list[0];
                
            }
            else if ((average < 5000) && (average > 3000))
            {
                img.color = list[2];
                
            }
            else if((average < 18000) && (average > 15000))
            {
                img.color = list[2];
            }
            else
            {
                img.color = list[1];
                
            }

            txt.text = average.ToString("N1") + " "+unit;
            
        }
        counter = counter + 1;

    }
}

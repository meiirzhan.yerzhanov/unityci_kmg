﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowAnimation : MonoBehaviour
{
    public GameObject flow;
    Animator flowAnimation;
    void Start()
    {
        flowAnimation = flow.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.name == "StopFlow")
        {
            flowAnimation.enabled = false;
        }
        if(col.name == "StartFlow")
        {
            flowAnimation.enabled = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfViewChange : MonoBehaviour
{
    public float fieldview1=60f;
    public float inclusiveMinimum=5f;
    public float inclusiveMaximum=120f;
    public GameObject Scheme;
    void Start()
    {
    }
    void Update()
    {
        if ((Input.GetAxis("Mouse ScrollWheel") < 0) && Scheme.active==false)
        {
        fieldview1=fieldview1+5f;
            if (fieldview1 >= inclusiveMaximum) {fieldview1=inclusiveMaximum;}
        GetComponent<Camera>().fieldOfView = fieldview1;   
        }
        if ((Input.GetAxis("Mouse ScrollWheel") > 0)&& Scheme.active==false)
         {
        fieldview1=fieldview1-5f;
            if (fieldview1 <= inclusiveMinimum) {fieldview1=inclusiveMinimum;}
        GetComponent<Camera>().fieldOfView = fieldview1;
        }
    }
}

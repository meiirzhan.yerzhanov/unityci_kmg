﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SitDownRigidBodyUPSV : MonoBehaviour
{
    public CharacterController CharacterController;    
    public float CCHeightStand=2.0f;
    public float CCHeightSit=1f;
    [SerializeField] private bool isDown;
    void Start()
    {
    CharacterController = GameObject.Find("RigidBodyFPSController").GetComponent<CharacterController>();
    }

    void Update()
    {   
        if(Input.GetKeyDown(KeyCode.LeftControl)) 
        {
            isDown = !isDown;
        }
        if (isDown)
        {
            CharacterController.height = CCHeightSit;
        }
        else 
        {
           CharacterController.height = CCHeightStand;
        }
        
        
    }
}
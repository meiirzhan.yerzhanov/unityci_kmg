﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorOnMiniMap : MonoBehaviour
{
    [SerializeField]private bool CursorLockedVar;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = (false);
        CursorLockedVar = (true);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1) && !CursorLockedVar)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = (false);
            CursorLockedVar = (true);
            GetComponent<FieldOfViewChange>().enabled= true;
        }
        else if (Input.GetKeyDown(KeyCode.Mouse1) && CursorLockedVar)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = (true);
            CursorLockedVar = (false);
            GetComponent<FieldOfViewChange>().enabled= false;
        }
    }

}
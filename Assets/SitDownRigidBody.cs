﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SitDownRigidBody : MonoBehaviour
{
    public CapsuleCollider CapsuleCollider;    
    public float CCHeightStand=2.5f;
    public float CCHeightSit=1f;
    void Start()
    {
    CapsuleCollider = GetComponent<CapsuleCollider>();
    }

    void Update()
    {   
        if(Input.GetKeyDown(KeyCode.LeftControl)) 
        {
            CapsuleCollider.height = CCHeightSit;
        }
        else 
        {
            CapsuleCollider.height = CCHeightStand;
        }
        
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DebugConsole : MonoBehaviour
{
    public TextMeshProUGUI logText;

    void OnEnable()
    {
        Application.logMessageReceived += LogCallback;
    }

    void LogCallback(string logString, string stackTrace, LogType type)
    {
        if(type != LogType.Warning){
            logText.text += logString + "\n";
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using System;


public class MetaDataText_v1 : MonoBehaviour
{
    public GameObject[] Row;
    public GameObject TableList; 
    public GameObject modelwithinfo;

    // line number
    public List<int> lines = new List<int>();

    void Start()
    {
        PiXYZ.Import.Metadata list = modelwithinfo.GetComponent<PiXYZ.Import.Metadata>();
        int peremennaya;
        peremennaya = Row.Length;
       
        for (int i=0; i<peremennaya ;i++)
        {
       
        TextMeshProUGUI[] Text = Row[i].GetComponentsInChildren<TextMeshProUGUI>();

        var list2 = list.getProperties();
        List<string> mylist = new List<string>();
        List<string> mylist2 = new List<string>();
        Dictionary<string, string>.KeyCollection keys = list2.Keys; 
        foreach (string a in keys)
        {
            if(a.StartsWith("Текст") || a.StartsWith("Размеры"))
            {
                mylist.Add(list2[a]);
                mylist2.Add(a);
            }
        }
            Text[1].text = mylist[i];
            Text[0].text = mylist2[i];
       
        //Text[1].text = mylist[lines[i]];
        //Text[0].text = mylist2[lines[i]];     
        }
    }
}
